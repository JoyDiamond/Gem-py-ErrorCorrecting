def do_decode_solution(self, data):
    correct = 0
    simple_data = [None] * 10

    for [i, row] in enumerate(data):
        if row == None:
            continue

        assert len(row) == 32

        content   = row[:16]
        signature = row[16:]

        chunk = ord(content[0])
        use   = ord(content[1])

        #<SOLUTION #1>
        if signature != hashlib.md5(content).digest():
            continue
        #</SOLUTION #1>

        correct += 1

        #<SOLUTION #2>
        simple_data[chunk] = tuple(ord(c)   for c in content[2 : 2 + use])
        #</SOLUTION #2>

    if correct < 7:
        return None

    return simple_decode(simple_data)
