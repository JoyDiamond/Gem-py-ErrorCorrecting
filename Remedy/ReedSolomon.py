#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#   Copyright Emin Martinian 2002.  See below for license terms.
#

license_doc = """
  This code was originally written by Emin Martinian (emin@allegro.mit.edu).
  You may copy, modify, redistribute in source or binary form as long
  as credit is given to the original author.  Specifically, please
  include some kind of comment or docstring saying that Emin Martinian
  was one of the original authors.  Also, if you publish anything based
  on this work, it would be nice to cite the original author and any
  other contributers.

  There is NO WARRANTY for this software just as there is no warranty
  for GNU software (although this is not GNU software).  Specifically
  we adopt the same policy towards warranties as the GNU project:

  BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM 'AS IS' WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.

  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.
"""


def produce_multiply_table():
    def find_degree(v):
        if v == 0:
            return 0

        result = -1

        while v:
            v = v >> 1
            result = result + 1

        return result


    def full_division_1(f, v, fDegree, vDegree):
        result = 0
        i = fDegree
        mask = 1 << i

        while i >= vDegree:
            if (mask & f):
                result = result ^ (1 << (i - vDegree))
                f ^= v << (i - vDegree)
                
            i = i - 1
            mask = mask >> 1

        return f


    def multiply_without_reducing(f, v):
        result = 0
        mask = 1
        i = 0

        while i <= 8:
            if mask & v: 
                result = result ^ f

            f = f << 1
            mask = mask << 1
            i = i + 1

        return result


    def full_multiply(f, v):
        m = multiply_without_reducing(f, v)

        return full_division_1(m, 285, find_degree(m), 8)


    multiply_table    = range(256)
    multiply_table[0] = [0] * 256

    range_256 = range(256)

    for i in range(1, 256):
        multiply_table[i] = tuple(full_multiply(i, j)   for j in range_256)

    return tuple(multiply_table)


def produce_divide_table():
    inverse_table = (
            0, 1, 142, 244, 71, 167, 122, 186, 173, 157, 221, 152, 61, 170, 93, 150, 216, 114, 192, 88, 224, 62,
            76, 102, 144, 222, 85, 128, 160, 131, 75, 42, 108, 237, 57, 81, 96, 86, 44, 138, 112, 208, 31, 74, 38,
            139, 51, 110, 72, 137, 111, 46, 164, 195, 64, 94, 80, 34, 207, 169, 171, 12, 21, 225, 54, 95, 248, 213,
            146, 78, 166, 4, 48, 136, 43, 30, 22, 103, 69, 147, 56, 35, 104, 140, 129, 26, 37, 97, 19, 193, 203, 99,
            151, 14, 55, 65, 36, 87, 202, 91, 185, 196, 23, 77, 82, 141, 239, 179, 32, 236, 47, 50, 40, 209, 17,
            217, 233, 251, 218, 121, 219, 119, 6, 187, 132, 205, 254, 252, 27, 84, 161, 29, 124, 204, 228, 176, 73,
            49, 39, 45, 83, 105, 2, 245, 24, 223, 68, 79, 155, 188, 15, 92, 11, 220, 189, 148, 172, 9, 199, 162, 28,
            130, 159, 198, 52, 194, 70, 5, 206, 59, 13, 60, 156, 8, 190, 183, 135, 229, 238, 107, 235, 242, 191,
            175, 197, 100, 7, 123, 149, 154, 174, 182, 18, 89, 165, 53, 101, 184, 163, 158, 210, 247, 98, 90, 133,
            125, 168, 58, 41, 113, 200, 246, 249, 67, 215, 214, 16, 115, 118, 120, 153, 10, 25, 145, 20, 63, 230,
            240, 134, 177, 226, 241, 250, 116, 243, 180, 109, 33, 178, 106, 227, 231, 181, 234, 3, 143, 211, 201,
            66, 212, 232, 117, 127, 255, 126, 253
        )


    divide_table    = range(256)
    divide_table[0] = ['NaN'] * 256

    range_256 = range(256)

    for i in xrange(1, 256):
        divide_table[i] = tuple(multiply_table[i][inverse_table[j]]   for j in range_256)

    return tuple(divide_table)


multiply_table = produce_multiply_table()
divide_table   = produce_divide_table()


def Multiply(i, j):
    return multiply_table[i][j]


def Divide(i, j):
    return divide_table[i][j]

    
def Add(x, y):
    return x ^ y


def Subtract(x, y):
    return x ^ y


import operator


class GenericMatrix(object):
    __slots__ = ((
        'add',
        'sub',
        'mul',
        'div',
        'eq',
        'str',
        'zeroElement',
        'identityElement',
        'rows',
        'cols',
        'data',
    ))

        
    def __init__(self, size=(2, 2), zeroElement=0.0, identityElement=1.0,
                 add=operator.__add__, sub=operator.__sub__,
                 mul=operator.__mul__, div=operator.__div__,
                 eq=operator.__eq__, str=lambda x:`x`,
                 fillMode='z'):
        self.add = add
        self.sub = sub
        self.mul = mul
        self.div = div
        self.eq = eq
        self.str = str
        self.zeroElement = zeroElement
        self.identityElement = identityElement
        self.rows, self.cols = size
        self.data = []

        def q(x, y, z):
            if (x):
                return y
            else:
                return z

        if (fillMode == 'e'):
            return
        elif (fillMode == 'z'):
            fillMode = lambda x, y: self.zeroElement
        elif (fillMode == 'i'):
            fillMode = lambda x, y: q(self.eq(x, y), self.identityElement, self.zeroElement)

        for i in range(self.rows):
            self.data.append(map(fillMode, [i] * self.cols, range(self.cols))) 


    def MakeSimilarMatrix(self, size, fillMode):
        return GenericMatrix(size=size, zeroElement=self.zeroElement,
                             identityElement=self.identityElement,
                             add=self.add, sub=self.sub,
                             mul=self.mul, div=self.div, eq=self.eq,
                             str=self.str,
                             fillMode=fillMode)
        

    def __repr__(self):
        m = 0
        # find the fattest element
        for r in self.data:
            for c in r:
                l = len(self.str(c))
                if l > m:
                    m = l
        f = '%%%ds' % (m + 1)
        s = '<matrix'
        for r in self.data:
            s = s + '\n'
            for c in r:
                s = s + (f % self.str(c))
        s = s + '>'
        return s

    def __setitem__ (self, (x, y), data):
        self.data[x][y] = data


    def Size (self):
        return (len(self.data), len(self.data[0]))

    
    def SetRow(self, r, result):
        assert len(result) == self.cols

        self.data[r] = list(result)


    def GetRow(self, r):
       return list(self.data[r])


    def Transpose(self):
        oldData = self.data
        self.data = []
        for r in range(self.cols):
            self.data.append([])
            for c in range(self.rows):
                self.data[r].append(oldData[c][r])
        rows = self.rows
        self.rows = self.cols
        self.cols = rows


    def Copy(self):
        result = self.MakeSimilarMatrix(size=self.Size(), fillMode='e')

        for r in self.data:
            result.data.append(list(r))
        return result


    def MulRow(self, r, m, start=0):
        row = self.data[r]

        for i in range(start, self.cols):
            row[i] = self.mul(row[i], m)

    def AddRow(self, i, j):
        self.data[j] = map(self.add, self.data[i], self.data[j])



    def MulAddRow(self, m, i, j):
        self.data[j] = map(self.add,
                           map(self.mul, [m] * self.cols, self.data[i]),
                           self.data[j])


    def LeftMulColumnVec(self, colVec):
        if (self.cols != len(colVec)):
            raise ValueError, 'dimension mismatch'
        result = range(self.rows)
        for r in range(self.rows):
            result[r] = reduce(self.add, map(self.mul, self.data[r], colVec))
        return result


    def FindRowLeader(self, startRow, c):
        for r in range(startRow, self.rows):
            if (not self.eq(self.zeroElement, self.data[r][c])):
                return r
        return - 1


    def PartialLowerGaussElim(self, rowIndex, colIndex, resultInv):
        lastRow = self.rows - 1

        while (rowIndex < lastRow):
            if (colIndex >= self.cols):
                return (rowIndex, colIndex)
            if (self.eq(self.zeroElement, self.data[rowIndex][colIndex])):
                # self[rowIndex,colIndex] = 0 so quit.
                return (rowIndex, colIndex)
            divisor = self.div(self.identityElement,
                               self.data[rowIndex][colIndex])
            for k in range(rowIndex + 1, self.rows):
                nextTerm = self.data[k][colIndex]
                if (self.zeroElement != nextTerm):
                    multiple = self.mul(divisor, self.sub(self.zeroElement,
                                                         nextTerm))
                    self.MulAddRow(multiple, rowIndex, k)
                    resultInv.MulAddRow(multiple, rowIndex, k)
            rowIndex = rowIndex + 1
            colIndex = colIndex + 1

        return (rowIndex, colIndex)



    def LowerGaussianElim(self, resultInv=''):
        if (resultInv == ''):
            resultInv = self.MakeSimilarMatrix(self.Size(), 'i')
            
        (rowIndex, colIndex) = (0, 0)
        lastRow = min(self.rows - 1, self.cols)
        lastCol = self.cols - 1
        
        while(rowIndex < lastRow and colIndex < lastCol):
            leader = self.FindRowLeader(rowIndex, colIndex)
            if (leader < 0):
                colIndex = colIndex + 1
                continue
            if (leader != rowIndex):
                resultInv.AddRow(leader, rowIndex)
                self.AddRow(leader, rowIndex)
            (rowIndex, colIndex) = (
                self.PartialLowerGaussElim(rowIndex, colIndex, resultInv))

        return resultInv


    def UpperInverse(self, resultInv=''):
        if (resultInv == ''):
            resultInv = self.MakeSimilarMatrix(self.Size(), 'i')

        lastCol = min(self.rows, self.cols)

        for colIndex in range(0, lastCol):
            if (self.zeroElement == self.data[colIndex][colIndex]):
                raise ValueError, 'matrix not invertible'

            divisor = self.div(self.identityElement, self.data[colIndex][colIndex])

            if (self.identityElement != divisor):
                self.MulRow(colIndex, divisor, colIndex)
                resultInv.MulRow(colIndex, divisor)

            for rowToElim in range(0, colIndex):
                multiple = self.sub(self.zeroElement, self.data[rowToElim][colIndex])
                self.MulAddRow(multiple, colIndex, rowToElim)
                resultInv.MulAddRow(multiple, colIndex, rowToElim)

        return resultInv
    

    def Inverse(self):
        workingCopy = self.Copy()
        result = self.MakeSimilarMatrix(self.Size(), 'i')
        workingCopy.LowerGaussianElim(result)
        workingCopy.UpperInverse(result)
        return result


class ReedSolomon(object):
    __slots__ = ((
        'n',
        'k',
        'encoder_matrix',
    ))


    def __init__(self, n, k):
        encoder_matrix = GenericMatrix((n, k), 0, 1, Add, Subtract, Multiply, Divide)
        encoder_matrix[0, 0] = 1

        for i in range(0, n):
            term = 1
            for j in range(0, k):
                encoder_matrix[i, j] = term
                term = Multiply(term, i)

        encoder_matrix.Transpose()
        encoder_matrix.LowerGaussianElim()
        encoder_matrix.UpperInverse()
        encoder_matrix.Transpose()

        self.n              = n
        self.k              = k
        self.encoder_matrix = encoder_matrix;


    def encode(self, data):
        assert len(data) == self.k, 'Encode: input data must be size k list.'
        
        return self.encoder_matrix.LeftMulColumnVec(data)


    def decode(self, data):
        n = self.n
        k = self.k

        if len(data) != n:
            raise ValueError, 'input must be a length n list'

        seen_locations = []
        seen_values = []

        for i in range(self.n):
            if (None != data[i]):
                seen_locations.append(i)
                seen_values.append(data[i])

            if len(seen_locations) == k:
                break
        else:
            raise ValueError, 'need at least k valid entries'
            
        limited_encoder = GenericMatrix((k, k), 0, 1, Add, Subtract, Multiply, Divide)

        for i in range(0, k):
            limited_encoder.SetRow(i, self.encoder_matrix.GetRow(seen_locations[i]))
            
        decoder_matrix = limited_encoder.Inverse()

        return decoder_matrix.LeftMulColumnVec(seen_values)
