#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
import  errno
import  hashlib


class MockRawSolidStateDevice(object):
    MAXIMUM_BLOCK_NUMBER = 15

    
    __slots__ = ((
        'blocks',                       #   A list of 16 blocks, lost blocks are stored as 'None'.
    ))


    def __init__(t):
        t.blocks = [' ' * 32] * (t.MAXIMUM_BLOCK_NUMBER + 1);


    def CORRUPT(t, block_number):
        data = [c   for c in t.read(block_number)]

        data[0]  = 'D'
        data[7]  = 'E'
        data[17] = 'A'
        data[27] = 'D'

        t.write(block_number, ''.join(data))

        
    def read(t, block_number):
        assert 0 <= block_number <= t.MAXIMUM_BLOCK_NUMBER

        result = t.blocks[block_number]

        if result == None:
            error = OSError(errno.EIO, 'memory block {} is lost'.format(block_number));

            print ('MockRawSolidStateDevice.read({}) => Raise {}'.format(block_number, error))

            raise error

        #print ('MockRawSolidStateDevice.read({}) => {!r}'.format(block_number, result))
        return result


    def WIPE(t, block_number):
        assert 0 <= block_number <= t.MAXIMUM_BLOCK_NUMBER

        t.blocks[block_number] = None


    def write(t, block_number, data):
        assert 0 <= block_number <= t.MAXIMUM_BLOCK_NUMBER
        assert len(data) == 32

        if t.blocks[block_number] == None:
            print ('MockRawSolidStateDevice.write({}, {!r}) => Raise OSError')
            raise OSError(errno.EIO, 'memory block {} is lost'.format(block_number));

        t.blocks[block_number] = data

        #print ('MockRawSolidStateDevice.write({}, {!r})'.format(block_number, data))



class MockSolidStateDevice(object):
    INDEX_BLOCK_NUMBER   = 7
    MAXIMUM_BLOCK_NUMBER = 14

    
    __slots__ = ((
        'raw_device',                   #   MockRawSolidStateDevice
        'mapping_table',                #   Maps virtual blocks to physical blocks
    ))


    def __init__(t, raw_device):
        t.raw_device    = raw_device
        t.mapping_table = None


    def read_mapping_table(t):
        assert t.mapping_table is None

        data      = t.raw_device.read(t.INDEX_BLOCK_NUMBER)
        content   = data[:16]
        signature = data[16:]

        if signature != hashlib.md5(content).digest():
            error = OSError(errno.EIO, 'mapping table is corrupt')

            print('MockSolidStateDevice.read_mapping_table => raise {}'.format(error))

            raise error

        assert content[15] == chr(0)

        t.mapping_table = tuple(ord(c)   for c in content[:15])

        assert t.INDEX_BLOCK_NUMBER not in t.mapping_table


    def read(t, block_number):
        assert 0 <= block_number <= t.MAXIMUM_BLOCK_NUMBER

        return t.raw_device.read(t.mapping_table[block_number])


    def write(t, block_number, data):
        assert 0 <= block_number <= t.MAXIMUM_BLOCK_NUMBER

        t.raw_device.write(t.mapping_table[block_number], data)


def create_MockRawSolidStateDevice():
    def write_mapping_table():
        mapping_table = ((9, 11, 6, 5, 13, 15, 1, 3, 0, 8, 12, 2, 14, 10, 4))

        content   = ''.join(chr(v)   for v in mapping_table) + '\0'
        signature = hashlib.md5(content).digest()

        raw_device.write(MockSolidStateDevice.INDEX_BLOCK_NUMBER, content + signature)

    raw_device = MockRawSolidStateDevice()

    write_mapping_table()

    return raw_device


def create_MockSolidStateDevice(raw_device):
    device = MockSolidStateDevice(raw_device)

    device.read_mapping_table()

    return device
