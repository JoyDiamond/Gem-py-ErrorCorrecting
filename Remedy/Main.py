#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
import hashlib

from    MockSolidStateDevice    import  create_MockRawSolidStateDevice
from    MockSolidStateDevice    import  create_MockSolidStateDevice
from    ReedSolomon             import  ReedSolomon


#
#   reed_solomon_encode
#       encode an array of 7 integers into an array of 10 integers
#
#   reed_solomon_decode:
#       decode an array of 10 integers into an array of 7 integers (up to 3 input values may be None)
#
reed_solomon = ReedSolomon(10, 7)

reed_solomon_encode = reed_solomon.encode
reed_solomon_decode = reed_solomon.decode


#
#   Explanation:
#
#       We want to write to a SolidStateDevice, a string, such that if parts of the underlying
#       flash memory become corrupt, we can recover our original string.
#
#       For testing purposes (of the code you are developing) we use a MockSolidStateDevice, that
#       has support for a total of 12 blocks, each 32 bytes.
#
#       The MockSolidStateDevice can develop both corruptions of the data stored to it, and lose
#       whole blocks of data (just like a real SolidStateDevice) can:
#
#           In addition, the MockSolidStateDevice internal index table can become corrupted; in
#           such a case, we still want to recover our origianl string.
#
#
#   Core Concepts:
#
#       We want to encode a string of data, such that if parts of the data is lost, we can recover the
#       original string.
#
#       The core concept to do this will be Reed Solomon encoding:
#
#               Converting an array of 7 integers to an array of of 10 integers.
#
#               Then any 3 integers can be "lost" & the original array of 7 integers can be recovered.
#
#   Sample code:
#
#       The sample code shows how to use the `reed_solomon_encode` and `reed_solomon_decode` functions.
#
#       The second version of the sample code shows how to convert a string into list of 10 tuples,
#       such that if up to three tuples are lost, we can recover the original string.
#
#       The third version of the sample code shows how to encode a string into a list of "blocks"
#       we can then write to a "block" device:
#
#           Each tuple has the following format:
#
#               1 byte: Chunk #
#               * bytes: Exact same data as produced by simple_encode
#               20 bytes: md5 signature.
#
#           The `decode` function works as follow:
#
#               Given a scambled set of 7 or more tuples ... 
#
#               Verify if each tuple is correct (by it's sha1 signature)
#
#               If less than 7 tuples are correct return 'none'.
#
#               If at least 7 tuples are correct, then return the orginal string
#
#                   This is done by creating an array of 10 tuples,
#                   putting the tuples in the proper order in this array; and then calling
#                   the decode function from encoder_sample_2
#
#   Task:
#
#       Your task is to complete three functions:
#
#           encode      - Writes the string to the solid state device
#
#           decode      - Reads the string from the solid state device
#
#           recover     - Recover's the string from the solid state device, when the solid state device
#                         internal index table becomes corrupted.
#
#       The format in which the data is stored is as follows:
#
#               1 byte: Chunk #             (need for `recover` to work properly).
#               1 byte: Length
#               14 bytes: Data
#               16 bytes: md5 signature
#
#       The `encode` function is already written.
#
#           Your task is to write the `decode` and `recover` functions.
#
def encoder_sample_1():
    expected = [44, 84, 104, 101, 32, 113, 117]
    data     = reed_solomon_encode(expected)

    #
    #   Wipe out any 3 elements ...
    #
    data[0] = None;
    data[2] = None;
    data[7] = None;

    actual = reed_solomon_decode(data)


    #
    #   Print:
    #       1.  Input string (expected)
    #       2.  Print out the encoded data (with part of it having been wiped out).
    #       3.  The decoded string (actual).
    #
    print('encoder_sample_1:')
    print('  Expected: {!r}'.format(expected))
    print('  Encoded:  {}'.format(data))
    print('  Decoded:  {!r}'.format(actual))


    #
    #   They had better match ...
    #
    assert actual == expected


def encoder_sample_2():
    #
    #   simple_encode:
    #
    #       Create as many 7 bytes vectors as needed to encode the string & encode each 7 byte vector.
    #       We encode the length of the string as the first element.
    #       The last vector is padded with 0's.
    #
    #       Returns a list of 10 tuples, each index in the tuple corrosponding to 7 characters in the
    #       original string (with the length of the string as the first element).
    #
    expected = "The quick brown fox jumps over the lazy dog."
    data     = simple_encode(expected)


    #
    #   Wipe out any 3 elements ...
    #
    data[0] = None;
    data[2] = None;
    data[7] = None;

    actual = simple_decode(data)

    #
    #   Print:
    #       1.  Input string (expected)
    #       2.  Print out the encoded data (with part of it having been wiped out).
    #       3.  The decoded string (actual).
    #
    print
    print('encoder_sample_2:')
    print('  Expected: {!r}'.format(expected))

    for [i, v] in enumerate(data):
        print('  Encoded #{}: {}'.format(i, v))

    print('  Decoded:  {!r}'.format(actual))


    #
    #   They had better match ...
    #
    assert actual == expected


def simple_encode(s):
    assert len(s) < 255

    vector = [len(s)] * 7
    index  = 1

    result = [[]   for v in range(10)]

    def add_vector():
        for [i, v] in enumerate(reed_solomon_encode(vector)):
            result[i].append(v)

    for c in s:
        vector[index] = ord(c)

        index += 1

        if index == 7:
            add_vector()
            index = 0

    if index:
        while index < 7:
            vector[index] = 0
            index += 1;

        add_vector()

    return [tuple(v)   for v in result]



def simple_decode(data):
    #
    #   simple_decode:
    #
    #       data is a list of 10 tuples (up to 3 of these tuples can be None instead).
    #       For each entry in the tuple, decode it into the 7 original values, and create a string.
    #       The first entry is the length of the string
    #
    assert len(data) == 10

    total = None

    for j in range(1, 10):
        row = data[j]

        if row is None:
            continue

        if total is None:
            total = len(row)
            continue

        assert total == len(row)

    chunk    = [None] * 10
    range_10 = range(10)
    s        = ''
    s_total  = None

    for i in xrange(total):
        for j in range(10):
            row = data[j]

            if row is None:
                continue

            chunk[j] = row[i]

        for v in reed_solomon_decode(chunk):
            if s_total == None:
                s_total = v
            else:
                s += chr(v)

    return s[:s_total]


def encode_3(s):
    result = simple_encode(s)

    for [i, row] in enumerate(result):
        s         = chr(i) + ''.join(chr(v)   for v in row)
        signature = hashlib.md5(s).digest()

        result[i] = (i,) + row + tuple(ord(s)   for s in signature)

    return result


def decode_3(data):
    assert len(data) == 10

    correct = 0
    simple_data = [None] * 10

    for [i, row] in enumerate(data):
        prefix_string = ''.join(chr(v)   for v in row[:-16])
        signature     = ''.join(chr(v)   for v in row[-16:])

        if signature != hashlib.md5(prefix_string).digest():
            print('Row {} is corrupt ... ignoring'.format(i))
            continue

        correct += 1
        i = row[0]
        simple_data[i] = row[1:-16]

    if correct < 7:
        return none

    return simple_decode(simple_data)


def encoder_sample_3():
    print
    print('encoder_sample_3:')

    expected = 'The quick brown fox jumps over the lazy dog.'
    data     = encode_3(expected)


    #
    #   Corrupt any 3 elements ...
    #
    for i in [0, 2, 7]:
        row = list(data[i])

        for j in range(0, len(row), 7):
            row[j] = 0

        data[i] = tuple(row)

    actual = decode_3(data)

    #
    #   Print:
    #       1.  Input string (expected)
    #       2.  The decoded string (actual).
    #
    print('  Expected: {!r}'.format(expected))
    print('  Decoded:  {!r}'.format(actual))


    #
    #   They had better match ...
    #
    assert actual == expected


CHEATING_test_number = 0


def test_solid_state_driver():
    expected = "The quick brown fox jumps over the lazy dog."

    raw_device   = create_MockRawSolidStateDevice()
    write_device = create_MockSolidStateDevice(raw_device)

    write_driver = SolidStateDriver(write_device)

    write_driver.encode(expected)


    def decode_test(test_number, expected = expected):
        global CHEATING_test_number

        CHEATING_test_number = test_number

        print('decode_test #{}:'.format(test_number))

        try:
            read_device = create_MockSolidStateDevice(raw_device)
        except OSError as e:
            #
            #   OOPS -- Will have to use the underlying raw_device, and recover data
            #
            read_device = None

            print('recode_test #{}: (since cannot decode)'.format(test_number))

        if read_device:
            solid_state_driver = SolidStateDriver(read_device)

            actual = solid_state_driver.decode()
        else:
            solid_state_driver = SolidStateDriver(raw_device)

            actual = solid_state_driver.recover()

        print('  Expected: {!r}'.format(expected))
        print('  Decoded:  {!r}'.format(actual))

        #
        #   They had better match ...
        #
        assert actual == expected


    decode_test(1)

    raw_device.CORRUPT(0)
    raw_device.CORRUPT(13)
    raw_device.WIPE(11)
    decode_test(2)

    raw_device.CORRUPT(7)
    decode_test(3)

    raw_device.WIPE(7)
    decode_test(4)

    raw_device.CORRUPT(1)
    decode_test(5, None)            #   Disk is so corrupt, cannot recover



class SolidStateDriver(object):
    __slots__ = ((
        'device',                       #   MockRawSolidStateDevice | MockSolidStateDevice
    ))


    def __init__(self, device):
        self.device = device


    def decode(self):
        data = []

        for block_number in range(10):
            try:
                block = self.device.read(block_number)
            except OSError as e:
                block = None

            data.append(block)

        #for [i, v] in enumerate(data):
        #    print('decode: data[{}]: {!r}'.format(i, v))

        return self.do_decode(data)


    def encode(self, s):
        result = simple_encode(s)

        for [i, row] in enumerate(result):
            content = chr(i) + chr(len(row)) + ''.join(chr(v)   for v in row)
            content += ' ' * (16 - len(content))
            
            signature = hashlib.md5(content).digest()

            self.device.write(i, content + signature)

        return result


    def recover(self):
        data = []

        for block_number in range(self.device.MAXIMUM_BLOCK_NUMBER + 1):
            try:
                block = self.device.read(block_number)
            except OSError as e:
                block = None

            data.append(block)

        #for [i, v] in enumerate(data):
        #    print('recover: data[{}]: {!r}'.format(i, v))

        return self.do_decode(data)


    def do_decode(self, data):
        correct = 0
        simple_data = [None] * 10

        for [i, row] in enumerate(data):
            if row == None:
                continue

            assert len(row) == 32

            content   = row[:16]
            signature = row[16:]

            chunk = ord(content[0])
            use   = ord(content[1])

            #
            #   TASK: FIX THIS, TO WORK PROPERLY, and FILL in simple_data
            #
            #   You have the following:
            #
            #   1.  row is a string of 16 characters.
            #
            #   2.  The first 16 characters are:
            #
            #           chunk               - Chunk #.  Use this as an index into simple_data
            #           use                 - Length of string to pass into simple_data
            #           row[2 : 2 + use]    - The string to pass store in simple_data[chunk]
            #
            #   3.  The next 16 characters are the md5 signature.
            #
            #       The following has to be true for the signatures to match:
            #
            #           content   = row[:16]
            #           signature = row[16:]
            #
            #           assert signature == hashlib.md5(content).digest()
            #
            #       If the signatures do *NOT* match, this row is corrupt, do not use it.
            #
            #           `simple_data` has been initialize to store `None` for unknown
            #           rows -- so to ignore a corrupt row, you only need to NOT
            #           store anything in `simple_data`
            #
            #  4.   Assuming the row is not corrupt, you need to store the into:
            #
            #           simple_data[chunk_number]
            #
            #       The ordinals of content[2: 2 + use]
            #
            #           content is a string
            #           simple_data (to use the reed_solomon_decode needs to have integers).
            #           Thus use `ord` on each character to convert it to an integer
            # 
            #

            #<FIX #1>
            #   Test the signature here properly
            #   If the signature does not match then `continue` (i.e.: ignore this row)
            #
            #   NOTE:
            #       The cheating code is in here, so you can see how the whole program works.
            #       Make sure to delete it & replace it with proper code!
            #
            cheating = {                                                    #   DELETE THIS
                    1 : [],                                                 #   DELETE THIS
                    2 : [4, 8],                                             #   DELETE THIS
                    3 : [0, 2, 4, 7, 10, 12, 13, 14],                       #   DELETE THIS
                    4 : [0, 2, 4, 10, 12, 13, 14],                          #   DELETE THIS
                    5 : [0, 1, 2, 4, 10, 12, 13, 14],                       #   DELETE THIS
                }                                                           #   DELETE THIS

            if i in cheating[CHEATING_test_number]:                         #   FIX THIS LINE to NOT CHEAT!
                continue
            #</FIX #1>

            correct += 1

            #<FIX #2>
            #   Store the proper data in simple_data[chunk]
            #
            #   NOTE:
            #       The cheating code is in here, so you can see how the whole program works.
            #       Make sure to delete it & replace it with proper code!
            cheating = {                                                    #   DELETE THIS
                    1 : {                                                   #   DELETE THIS
                        0 : [0, (44, 105, 119, 106, 118, 32, 111)],         #   DELETE THIS
                        1 : [1, (84, 99, 110, 117, 101, 108, 103)],         #   DELETE THIS
                        2 : [2, (104, 107, 32, 109, 114, 97, 46)],          #   DELETE THIS
                        3 : [3, (101, 32, 102, 112, 32, 122, 0)],           #   DELETE THIS
                        4 : [4, (32, 98, 111, 115, 116, 121, 0)],           #   DELETE THIS
                        5 : [5, (113, 114, 120, 32, 104, 32, 0)],           #   DELETE THIS
                        6 : [6, (117, 111, 32, 111, 101, 100, 0)],          #   DELETE THIS
                        7 : [7, (81, 62, 104, 62, 56, 106, 38)],            #   DELETE THIS
                        8 : [8, (17, 125, 112, 76, 59, 228, 201)],          #   DELETE THIS
                        9 : [9, (176, 225, 30, 130, 141, 79, 157)],         #   DELETE THIS
                    },                                                      #   DELETE THIS
                    2 : {                                                   #   DELETE THIS
                        0 : [0, (44, 105, 119, 106, 118, 32, 111)],         #   DELETE THIS
                        2 : [2, (104, 107, 32, 109, 114, 97, 46)],          #   DELETE THIS
                        3 : [3, (101, 32, 102, 112, 32, 122, 0)],           #   DELETE THIS
                        5 : [5, (113, 114, 120, 32, 104, 32, 0)],           #   DELETE THIS
                        6 : [6, (117, 111, 32, 111, 101, 100, 0)],          #   DELETE THIS
                        7 : [7, (81, 62, 104, 62, 56, 106, 38)],            #   DELETE THIS
                        9 : [9, (176, 225, 30, 130, 141, 79, 157)],         #   DELETE THIS
                    },                                                      #   DELETE THIS
                    3 : {                                                   #   DELETE THIS
                        1 : [6, (117, 111, 32, 111, 101, 100, 0)],          #   DELETE THIS
                        3 : [7, (81, 62, 104, 62, 56, 106, 38)],            #   DELETE THIS
                        5 : [3, (101, 32, 102, 112, 32, 122, 0)],           #   DELETE THIS
                        6 : [2, (104, 107, 32, 109, 114, 97, 46)],          #   DELETE THIS
                        8 : [9, (176, 225, 30, 130, 141, 79, 157)],         #   DELETE THIS
                        9 : [0, (44, 105, 119, 106, 118, 32, 111)],         #   DELETE THIS
                        15 : [5, (113, 114, 120, 32, 104, 32, 0)],          #   DELETE THIS
                    },                                                      #   DELETE THIS
                    4 : {                                                   #   DELETE THIS
                        1 : [6, (117, 111, 32, 111, 101, 100, 0)],          #   DELETE THIS
                        3 : [7, (81, 62, 104, 62, 56, 106, 38)],            #   DELETE THIS
                        5 : [3, (101, 32, 102, 112, 32, 122, 0)],           #   DELETE THIS
                        6 : [2, (104, 107, 32, 109, 114, 97, 46)],          #   DELETE THIS
                        8 : [9, (176, 225, 30, 130, 141, 79, 157)],         #   DELETE THIS
                        9 : [0, (44, 105, 119, 106, 118, 32, 111)],         #   DELETE THIS
                        15 : [5, (113, 114, 120, 32, 104, 32, 0)],          #   DELETE THIS
                    },                                                      #   DELETE THIS
                    5 : {                                                   #   DELETE THIS
                        3 : [7, (81, 62, 104, 62, 56, 106, 38)],            #   DELETE THIS
                        5 : [3, (101, 32, 102, 112, 32, 122, 0)],           #   DELETE THIS
                        6 : [2, (104, 107, 32, 109, 114, 97, 46)],          #   DELETE THIS
                        8 : [9, (176, 225, 30, 130, 141, 79, 157)],         #   DELETE THIS
                        9 : [0, (44, 105, 119, 106, 118, 32, 111)],         #   DELETE THIS
                        15 : [5, (113, 114, 120, 32, 104, 32, 0)],          #   DELETE THIS
                    },                                                      #   DELETE THIS
                }                                                           #   DELETE THIS

            simple_data[cheating[CHEATING_test_number][i][0]] = cheating[CHEATING_test_number][i][1]    #FIX THIS LINE to NOT CHEAT!
            #</FIX #2>

        if correct < 7:
            return None

        return simple_decode(simple_data)


encoder_sample_1()
encoder_sample_2()
encoder_sample_3()
test_solid_state_driver()
